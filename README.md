# Low-level Spinnaker

The repository includes examples and projects for low-level spinnaker code.

In order to build and run these projects a repository for easy instalation of the tools is created. See [Spinnaker-setup](https://gitlab.esrl.dk/SDU-embedded/neuromorphic/Spinnaker-setup).

When the Spinnaker tools is set up, clone the repository into the shared folder.

    cd <spinnaker-setup>/shared/
    git clone https://gitlab.esrl.dk/SDU-embedded/neuromorphic/low-level-spinnaker.git
    
## Low-level-tools usage (ybug / tubotron)

The low-level tools give acces to the different CPUs on the spinnaker system and the routing tables.

Every time the low-level spinnaker tools is used, the setup file in spinnaker-tools directory have to be sourced.

Source the setup file from the spinnaker tool directory

    cd ~/SupportScripts/spinnaker_tools/
    source setup

### Tubotron

When you have sourced the setup file, you can run tubotron.

Tubotron is a udp receiver application which receives messages from the spinnaker system and prints out in a gui.

The tool opens a small pane for each active core which sends out messages to the tubotron server.

This can be very useful for debugging when developping bare metel c-code for the spinnaker system.

When the setup file is sourced, Tubotron can be launched from commandline. It is recommended to start the tool as a background process which means it does not take up the cli.

    tubotron &

### Ybug

When you have sourced the setup file, you can run ybug:

    ybug 192.168.240.253 # runnung ybug with target system on 192.168.240.253
    
The response should be the version of ybug and a interactive prompt, like this:

    # ybug - version 3.1.1
    192.168.240.253:0,0,0 > 

The first line is the version of ybug and the second line is the entry point of the following commands.

Ybug have a "target" which is the address of the current selected chip. This way we don't need to specifi the target chip of every command but we need to make sure we have selected the right chip.

Now you should be able to boot the spinnaker board by typing the command 'boot'.

    boot

For this command there is three outcomes:

1. Can't boot

    error: too many retries
    
This means you don't have access to the Spinnaker board via the network.

2. Already booted

    Warning: Already booted.
    Booted SC&MP 3.1.1 on 4 chips

Ass stated, the system is already booted nothing will be done and you can continue.

3. Booted successfully

    Booted SC&MP 3.1.1 on 4 chips
    
SC&MP booted successfully. You are now ready.

### SP
First there is the ip address: 192.168.240.253
Then we have the X-address of the chip: 0
Then we have the Y-address of the chip: 0
And in the end we have the cpu address: 0


The command SP can be used to change the entry.

The command:

    SP <z>

Will make the entry pointing on core 'z' of the current selected chip. Where 'z' is a numher in the range 0-17.

The command:

    SP <x> <y>

Will make the entry pointing at core 0 on chip 'x', 'y'. Where 'x' and 'y' is a number in the range 0-1 for the Spinn3 board we are using.

Lastly we can select a specific core at a specific chip by the command:

    SP <x> <y> <z>

### PS

The command 'ps' shows the status of all cores on the selected chip. Another chip can be specified as agument

    ps

    ps <x> <y>
    
### RTR_DUMP

The command 'rtr_dump' shows the active routing entries in the routing table.
    
    rtr_dump
    
As default there us a route for key 0xffff5555 to core 0 which is used to route packets of system information to the monitor core.

The output should look like this:

    Entry  Route       (Core) (Link)  Key       Mask      AppID  Core
    -----  765432109876543210 543210  ---       ----      -----  ----
    
       0:  000000000000000001 000111  ffff5555  ffffffff      0     0
      FR:  000000000000000000 000000

### APP_LOAD

The command app_load is used to load application onto the spinnaker system from a host machine.

    app_load <app> <chip> <cores> <id>

Where:
- app is the executable file (.aplx).
- chip is the chip where the program should run ('.' for the current entry chip).
- cores is the core/cores where the program should run(in range from 1 to 17). In case the program should run on multyple cores on the samme chip a range can be specified(fx. 5-8).
- id is the app id i range of 16-99

Examples:

    app_load test.aplx . 1 16 # starting app with id 16 on core 1 of current selected chip

    app_load test.aplx . 4-9 18 # starting app with id 18 on core 4-9 on curent selected chip

    app_load test.aplx @1,1 1 16 # starting app with id 16 on core 1 on chip 1,1

### APP_STOP

The command app_stop simply stops all application with the specified app id and remove all routing entries which is ascosiated with that app id on all chips in the spinnaker system.

    app_stop

### APP_SIG

The command app_sig sends a signal to one or multyple cores at the same time. This is usualy used in order to syncrounize programs on different cores/chips.

Examples:

    app_sig all 16 sync0 # sends out sync0 signal to programs width app id 16, on all cores, on all chips

    app_sig @1,1 18 sync1 # sends out sync1 signal to programs width app id 18, on all cores, on chip 1,1
 
### SLEEP

The command sleep sleeps for 't' seconds.

    sleep <t>

Where t can be a float or an integer.

### Run scripts

You can create a list of commands in a file and run it as a script in ybug:

    @ test.ybug