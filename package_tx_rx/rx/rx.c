#include <sark.h>

// This test application receives a package and print it using IO_STD which can be obtained using tubotron.

// This function is called when a package is received
void pkt_proc(uint key, uint data){
	io_printf(IO_STD, "Received package: Key = %u, Data = %u\n", key, data);
}

void c_main(void){
	// Load app and core details
	uint core_id = sark_core_id();
	uint app_id = sark_app_id();
	char *app_name = (char *) &sark.vcpu->app_name;
	
	io_printf(IO_BUF, "Running app: Name = \"%s\",Core id = %d, App id = %d\n", app_name, core_id, app_id);

	// Setup package event
	event_register_queue(pkt_proc, EVENT_RXPKT, SLOT_0, PRIO_0);	
	event_register_pkt(16, SLOT_1);

	// Start event handling when sync0 is received
	uint rc = event_start(0, 0, SYNC_WAIT);

	io_printf(IO_BUF, "Terminated - RC %d\n", rc);
}
