This is a simple test of a single multicast package transmision.

The tx app and the two rx apps runs on the samme chip, and the routing is done from the tx app.

In order to run the test:
Source the setup file:
	source /spinnaker_tools_3.1.1/setup
Start tubotron
	tubotron &
Start ybug:
	ybug 192.168.240.253
From ybug, boot the spinnaker if it is not booted:
	boot
From ybug, load the ybug test script:
	@ test.ybug
In order to clean up the board run the stop script:
	@ stop.ybug
