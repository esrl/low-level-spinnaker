#include <sark.h>

// Package key
#define PKT_KEY		0x00000001

// Package data
#define PKT_DATA	0x00000005

// Routing mask
#define RTR_MASK	0xffffffff

#define RECEIVER_CORE_1 2
#define RECEIVER_CORE_2 3

//	This test application create a multicast route on the router table to core 2 and core 3 and transmits a packet with the routed key.

// This timer process starts when the signal "sync0" is reeived.
void timer_proc(uint data, uint none){

	uint key = PKT_KEY;
	io_printf(IO_STD, "Sending package: Key = %u, Data = %u\n", key, data);

	// Sending package
	pkt_tx_kd(key, data);

	timer_schedule_proc(timer_proc, data + 1, 0, 1000000);
}

void c_main(void){
	// Load app and core details
	uint core_id = sark_core_id();
	uint app_id = sark_app_id();
	char *app_name = (char *) &sark.vcpu->app_name;

	io_printf(IO_BUF, "Running app: Name = \"%s\",Core id = %d, App id = %d\n", app_name, core_id, app_id);

	// Setting up route entry
	uint e = rtr_alloc(1);
	if (e == 0) rt_error(RTE_ABORT);
	uint mask = RTR_MASK;
	uint route = MC_LINK_ROUTE(3);
	io_printf(IO_STD, "Setting route: Entry = %u, Key = %u, Mask = %u, Route = %u\n", e, core_id, mask, route);
	rtr_mc_set(e, core_id, mask, route);

	// Setup package event
	event_register_pkt(16, SLOT_1);

	// Setup timer event
	event_register_timer(SLOT_2);

	// Add timer event to event queue
	event_queue_proc(timer_proc, 0, 0, PRIO_0);

	// Start event handlingwhen sync0 is received
	uint rc = event_start(0, 0, SYNC_WAIT);

	io_printf(IO_BUF, "Terminated - RC %d\n", rc);
}
