#include <iomanip>
#include <string.h>
#include <fstream>
#include <unistd.h>
#include <vector>

#define SARK_VERSION 311

#include "../headers/SDP.hpp"

int main(){

  char* ip = "192.168.240.253";

  SDP sdp(2001, 17893, ip);
  sdp.sender().x = 0xff;
  sdp.sender().y = 0xff;
  sdp.sender().cpu = 0xff;
  sdp.sender().port = 0xff;
  sdp.receiver().x = 0x01;
  sdp.receiver().y = 0x01;
  sdp.receiver().cpu = 0x01;
  sdp.receiver().port = 0x02;

  uint32_t key = 0x00000001;

  SDP::Packet packet(sdp);
  packet.setData((void*)&key, sizeof(key));
  std::cout << packet << std::endl;
  packet.printRaw();
  sdp.send(&packet);
  return 0;
}
