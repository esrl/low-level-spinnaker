#define SARK_VERSION 311

#include "../headers/SDP.hpp"

#include <iomanip>
#include <string.h>

#define PAD_0 0
#define PAD_1 1
#define FLAGS 2
#define TAG 3
#define DEST_PORT_CPU 4
#define SRCE_PORT_CPU 5
#define DEST_X 6
#define DEST_Y 7
#define SRCE_X 8
#define SRCE_Y 9

#define SCP_HEADER 10

#define RETURN_CODE 10
#define SEQUENCE 12
#define ARG_1 14
#define ARG_2 18
#define ARG_3 22

#define DATA 26

#define DATA_SIZE DATA + SDP_DATA_SIZE

#define VERSION_COMMAND_SIZE 4
#define VERSION_RESPONSE_SIZE 16 + 256

#define READ_COMMAND_SIZE 16
#define READ_RESPONSE_SIZE 4

#define WRITE_COMMAND_SIZE 32
#define WRITE_RESPONSE_SIZE 4

// #define RUN_COMMAND_SIZE 8
// #define RUN_RESPONSE_SIZE 4
//
// #define APLX_COMMAND_SIZE 8
// #define APLX_RESPONSE_SIZE 4

SDP::SDP(uint16_t localPort, uint16_t externalPort, char* externalIp){
  if ((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) throw("Error while creating the socket!");

  memset(&localAddr, 0, sizeof(struct sockaddr_in));
  localAddr.sin_family = AF_INET;
  localAddr.sin_port = htons(localPort);
  localAddr.sin_addr.s_addr = htonl(INADDR_ANY);

  if (bind(sock, (struct sockaddr *)&localAddr, sizeof(struct sockaddr))) throw("Error while binding!");

  memset(&externalAddr, 0, sizeof(struct sockaddr_in));
  externalAddr.sin_family = AF_INET;
  externalAddr.sin_port = htons(externalPort);
  externalAddr.sin_addr.s_addr = inet_addr(externalIp);

  slen = sizeof(externalAddr);
}

SDP::Address& SDP::sender(){
  return tx;
}

SDP::Address& SDP::receiver(){
  return rx;
}

void SDP::send(SDP::Packet* data){
  if (sendto(sock, (void*)data, data->size() + SDP_HEADER_SIZE + SCP_HEADER_SIZE, 0, (struct sockaddr *) &externalAddr, slen) == -1) throw("Error while sending packet!");
}
void SDP::sendCommand(SDP::Packet* cmd, SDP::Packet* res){
  if (sendto(sock, (void*)cmd, cmd->size() + SDP_HEADER_SIZE + SCP_HEADER_SIZE, 0, (struct sockaddr*) &externalAddr, slen) == -1) throw("Error while sending packet!");
  int16_t size = recvfrom(sock, (void*)res, sizeof(SDP::Packet), 0, (struct sockaddr*)&externalAddr, (socklen_t*)&slen);
  if (size == -1) throw("Error while receiving packet!");
  res->size() = size - SDP_HEADER_SIZE - SCP_HEADER_SIZE;
}

SDP::Packet::Packet(){
  setFlagsTag(0x87, 0xff);
  setDest(0x00, 0x00, 0x01, 0x01);
  setSource(0x00, 0x00, 0xff, 0xff);
  setCmd(0x0000, 0x0000, 0x00000000, 0x00000000, 0x00000000);
}

SDP::Packet::Packet(SDP& sdp){
  setFlagsTag(0x87, 0xff);
  setDest(sdp.receiver().x, sdp.receiver().y, sdp.receiver().cpu, sdp.receiver().port);
  setSource(sdp.sender().x, sdp.sender().y, sdp.sender().cpu, sdp.sender().port);
  setCmd(0x0000, 0x0000, 0x00000000, 0x00000000, 0x00000000);
}

void SDP::Packet::setFlagsTag(uint8_t _flags, uint8_t _tag){
  data[FLAGS] = _flags;
  data[TAG] = _tag;
}

void SDP::Packet::setDest(uint8_t _destX, uint8_t _destY, uint8_t _destCPU, uint8_t _destPort){
  data[DEST_PORT_CPU] = (_destPort << 5) | (_destCPU & 0x1f);
  data[DEST_X] = _destX;
  data[DEST_Y] = _destY;
}

void SDP::Packet::setDest(SDP::Address& addr){
  data[DEST_PORT_CPU] = (addr.port << 5) | (addr.cpu & 0x1f);
  data[DEST_X] = addr.x;
  data[DEST_Y] = addr.y;
}

void SDP::Packet::setSource(uint8_t _sourceX, uint8_t _sourceY, uint8_t _sourceCPU, uint8_t _sourcePort){
  data[SRCE_PORT_CPU] = (_sourcePort << 5) | (_sourceCPU & 0x1f);
  data[SRCE_X] = _sourceX;
  data[SRCE_Y] = _sourceY;
}

void SDP::Packet::setSource(SDP::Address& addr){
  data[SRCE_PORT_CPU] = (addr.port << 5) | (addr.cpu & 0x1f);
  data[SRCE_X] = addr.x;
  data[SRCE_Y] = addr.y;
}

void SDP::Packet::setCmd(uint16_t _returnCode, uint16_t _sequence, uint32_t _arg1, uint32_t _arg2, uint32_t _arg3){
  uint16_t* ptr16;
  uint32_t* ptr32;
  ptr16 = (uint16_t*)&data[RETURN_CODE];
  *ptr16 = _returnCode;
  ptr16 = (uint16_t*)&data[SEQUENCE];
  *ptr16 = _sequence;
  ptr32 = (uint32_t*)&data[ARG_1];
  *ptr32 = _arg1;
  ptr32 = (uint32_t*)&data[ARG_2];
  *ptr32 = _arg2;
  ptr32 = (uint32_t*)&data[ARG_3];
  *ptr32 = _arg3;
}

void SDP::Packet::setCmd(void* ptr, uint16_t size){
  memcpy((void*)&data[SCP_HEADER], ptr, size);
}

void SDP::Packet::setData(void* ptr, uint16_t size){
  memcpy(&data[DATA], ptr, size);
  data[DATA_SIZE] = size;
}

uint8_t& SDP::Packet::flags(){
  return data[FLAGS];
}

uint8_t& SDP::Packet::tag(){
  return data[TAG];
}

uint8_t& SDP::Packet::dest_port_cpu(){
  return data[DEST_PORT_CPU];
}

uint8_t& SDP::Packet::srce_port_cpu(){
  return data[SRCE_PORT_CPU];
}

uint16_t& SDP::Packet::dest_addr(){
  return (uint16_t&)data[DEST_X];
}

uint16_t& SDP::Packet::srce_addr(){
  return (uint16_t&)data[SRCE_X];
}

uint16_t& SDP::Packet::cmd_rc(){
  return (uint16_t&)data[RETURN_CODE];
}

uint16_t& SDP::Packet::seq(){
  return (uint16_t&)data[SEQUENCE];
}

uint32_t& SDP::Packet::arg1(){
  return (uint32_t&)data[ARG_1];
}

uint32_t& SDP::Packet::arg2(){
  return (uint32_t&)data[ARG_2];
}

uint32_t& SDP::Packet::arg3(){
  return (uint32_t&)data[ARG_3];
}

uint8_t* SDP::Packet::dataPtr(){
  return &data[DATA];
}

uint16_t& SDP::Packet::size(){
  return *(uint16_t*)&data[DATA_SIZE];
}

void SDP::Packet::print(std::ostream& os){
  os << "=== SDP Header ===" << std::endl;
  os << std::hex << std::setfill('0') << std::setw(1) << "Flags:      \t" << (int)data[FLAGS] << std::endl;
  os << std::hex << std::setfill('0') << std::setw(2) << "Tag:        \t" << (int)data[TAG] << std::endl;
  os << "------------\tX\tY\tCPU\tPort" << std::endl;
  os << std::hex << std::setfill('0') << std::setw(2) << "Destination:\t" << (int)data[DEST_X] << "\t" << (int)data[DEST_Y] << "\t" << (int)(data[DEST_PORT_CPU] & 0x1f) << "\t" << (int)(data[DEST_PORT_CPU] >> 5) << std::endl;
  os << std::hex << std::setfill('0') << std::setw(2) << "Source:     \t" << (int)data[SRCE_X] << "\t" << (int)data[SRCE_Y] << "\t" << (int)(data[SRCE_PORT_CPU] & 0x1f) << "\t" << (int)(data[SRCE_PORT_CPU] >> 5) << std::endl;
  os << "=== SCP Header ===" << std::endl;
  os << std::hex << std::setfill('0') << std::setw(4) << "Return code:\t" << (int)data[RETURN_CODE] << std::endl;
  os << std::hex << std::setfill('0') << std::setw(4) << "Sequence:   \t" << (int)data[SEQUENCE] << std::endl;
  os << std::hex << std::setfill('0') << std::setw(8) << "Arg 1:      \t" << (int)data[ARG_1] << std::endl;
  os << std::hex << std::setfill('0') << std::setw(8) << "Arg 2:      \t" << (int)data[ARG_2] << std::endl;
  os << std::hex << std::setfill('0') << std::setw(8) << "Arg 3:      \t" << (int)data[ARG_3] << std::endl;
  os << "===    Data    ===" << std::endl;
  os << std::hex << std::setfill('0') << std::setw(2) << "Size:       \t" << (int)data[DATA_SIZE] << std::endl;
  for (int i = 0; i < 16; i++){
    os << std::setfill('0') << std::setw(4) << i * 16 << "  ";
    for (int j = 0; j < 16; j++){
      os << std::hex << std::setfill('0') << std::setw(2) << (int)data[DATA + i * 16 + j] << " ";
    }
    os << std::endl;
  }
}

void SDP::Packet::printRaw(){
  for (uint i = 0; i < sizeof(Packet); i++){
    if (i % 16 == 0) std::cout << std::setfill('0') << std::setw(4) << i * 16 << "  ";
      std::cout << std::hex << std::setfill('0') << std::setw(2) << (int)data[i] << " ";
    if (i % 16 == 15) std::cout << std::endl;
  }
  std::cout << std::endl;
}

SCP::VersionResponse SDP::sendVersionCommand(){
  commandPacket.setDest(rx);
  SCP::VersionCommand command;
  SCP::VersionResponse response;
  memcpy((void*)&commandPacket.cmd_rc(), (void*)&command, VERSION_COMMAND_SIZE);
  commandPacket.size() = 0x00;
  sendCommand(&commandPacket, &responsePacket);
  memcpy((void*)&response, (void*)&responsePacket.cmd_rc(), VERSION_RESPONSE_SIZE);
  return response;
}

SCP::ReadResponse SDP::sendReadCommand(uint32_t address, uint32_t length, uint32_t type){
  commandPacket.setDest(rx);
  SCP::ReadCommand command;
  SCP::ReadResponse response;
  command.address = address;
  command.length = length;
  command.type = type;
  memcpy((void*)&commandPacket.cmd_rc(), (void*)&command, READ_COMMAND_SIZE);
  commandPacket.size() = 0x00;
  sendCommand(&commandPacket, &responsePacket);
  memcpy((void*)&response, (void*)&responsePacket.cmd_rc(), READ_RESPONSE_SIZE + command.length);
  response.length = command.length;
  return response;
}

SCP::WriteResponse SDP::sendWriteCommand(uint32_t address, uint32_t length, uint32_t type, void* data){
  throw("There is a bug in this function, which occur on large packet sizes!");
  commandPacket.setDest(rx);
  SCP::WriteCommand command;
  SCP::WriteResponse response;
  command.address = address;
  command.length = length;
  command.type = type;
  memcpy((void*)&commandPacket.cmd_rc(), (void*)&command, WRITE_COMMAND_SIZE);
  memcpy((void*)commandPacket.dataPtr(), data, length);
  commandPacket.size() = length;
  sendCommand(&commandPacket, &responsePacket);
  memcpy((void*)&response, (void*)&responsePacket.cmd_rc(), WRITE_RESPONSE_SIZE);
  return response;
}

// SCP::RunResponse SDP::sendRunCommand(uint32_t address){
//   commandPacket.setDest(rx);
//   SCP::RunCommand command;
//   SCP::RunResponse response;
//   command.address = address;
//   memcpy((void*)&commandPacket.cmd_rc(), (void*)&command, RUN_COMMAND_SIZE);
//   commandPacket.size() = 0x00;
//   sendCommand(&commandPacket, &responsePacket);
//   memcpy((void*)&response, (void*)&responsePacket.cmd_rc(), RUN_RESPONSE_SIZE);
//   return response;
// }
//
// SCP::APLXResponse SDP::sendAPLXCommand(uint32_t address){
//   commandPacket.setDest(rx);
//   SCP::APLXCommand command;
//   SCP::APLXResponse response;
//   command.address = address;
//   memcpy((void*)&commandPacket.cmd_rc(), (void*)&command, APLX_COMMAND_SIZE);
//   commandPacket.size() = 0x00;
//   sendCommand(&commandPacket, &responsePacket);
//   memcpy((void*)&response, (void*)&responsePacket.cmd_rc(), APLX_RESPONSE_SIZE);
//   return response;
// }

std::ostream& operator<<(std::ostream& os, SDP::Packet& packet){
  packet.print(os);
  return os;
}

std::ostream& operator<<(std::ostream& os, SCP::VersionResponse& response){
  os << std::dec << std::setfill('0') << std::setw(4) << "Return code: \t" << response.rc_ok << std::endl;
  os << std::dec << std::setfill('0') << std::setw(4) << "Sequence:    \t" << response.seq << std::endl;
  os << std::hex << std::setfill('0') << std::setw(4) << "P2P address: \t" << response.p2p_addr << std::endl;
  os << std::dec << std::setfill('0') << std::setw(2) << "Phys address:\t" << (int)response.phys_cpu << std::endl;
  os << std::dec << std::setfill('0') << std::setw(2) << "Virt address:\t" << (int)response.virt_cpu << std::endl;
  os << std::dec << std::setfill('0') << std::setw(4) << "Version:     \t" << response.version << std::endl;
  os << std::dec << std::setfill('0') << std::setw(4) << "Buffer size: \t" << response.buffer_size << std::endl;
  os << std::dec << std::setfill('0') << std::setw(4) << "Build date:  \t" << response.build_date << std::endl;
  os << "Version str: \t" << response.version_str << std::endl;
  return os;
}

std::ostream& operator<<(std::ostream& os, SCP::ReadResponse& response){
  os << std::dec << std::setfill('0') << std::setw(4) << "Return code: \t" << response.rc_ok << std::endl;
  os << std::dec << std::setfill('0') << std::setw(4) << "Sequence:    \t" << response.seq << std::endl;
  os << "===    Data    ===" << std::endl;
  os << std::dec << "Size:       \t" << (int)response.length << std::endl;
  for (uint i = 0; i < response.length; i++){
    if (i % 16 == 0) std::cout << std::setfill('0') << std::setw(4) << i * 16 << "  ";
      os << std::hex << std::setfill('0') << std::setw(2) << (int)response.data[i] << " ";
    if (i % 16 == 15) std::cout << std::endl;
  }
  return os;
}

std::ostream& operator<<(std::ostream& os, SCP::WriteResponse& response){
  os << std::dec << std::setfill('0') << std::setw(4) << "Return code: \t" << response.rc_ok << std::endl;
  os << std::dec << std::setfill('0') << std::setw(4) << "Sequence:    \t" << response.seq << std::endl;
  return os;
}

// std::ostream& operator<<(std::ostream& os, SCP::RunResponse& response){
//   os << std::dec << std::setfill('0') << std::setw(4) << "Return code: \t" << response.rc_ok << std::endl;
//   os << std::dec << std::setfill('0') << std::setw(4) << "Sequence:    \t" << response.seq << std::endl;
//   return os;
// }
//
// std::ostream& operator<<(std::ostream& os, SCP::APLXResponse& response){
//   os << std::dec << std::setfill('0') << std::setw(4) << "Return code: \t" << response.rc_ok << std::endl;
//   os << std::dec << std::setfill('0') << std::setw(4) << "Sequence:    \t" << response.seq << std::endl;
//   return os;
// }
