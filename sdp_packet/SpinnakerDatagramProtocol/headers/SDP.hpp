/*
  This library is developed in order to ease the interfacing between a spinnaker
  machine and a host machine.

  The library provide full functionality of Spinnaker Datagram Protocol (SDP)
  including Spinnaker Command Protocol (SCP).

  TODO SCP
*/

#ifndef SDP_HPP
#define SDP_HPP

#ifndef SARK_VERSION
#error "SARK_VERSION have to be defined"
#endif

#include <arpa/inet.h>
#include <sys/socket.h>
#include <iostream>

#define SDP_HEADER_SIZE 10
#define SCP_HEADER_SIZE 16
#define SDP_DATA_SIZE   256
#define SDP_END_HEADER  1

#define SCP_CMD_VERSION 0x0000
#define SCP_CMD_RUN     0x0001
#define SCP_CMD_READ    0x0002
#define SCP_CMD_Write   0x0003
#define SCP_CMD_APLX    0x0004

namespace SCP{
  struct VersionCommand{
    uint16_t cmd_version = SCP_CMD_VERSION;
    uint16_t seq = 0x0000;
  };
  struct VersionResponse{
    uint16_t rc_ok;
    uint16_t seq;
    uint16_t p2p_addr;
    uint8_t phys_cpu;
    uint8_t virt_cpu;
    uint16_t version;
    uint16_t buffer_size;
    uint32_t build_date;
    char version_str[256];
  };

#if SARK_VERSION >= 200
  struct RunCommand{
    uint16_t cmd_run = SCP_CMD_RUN;
    uint16_t seq = 0x0000;
    uint32_t address = 0x00000000; // start address
  };
  struct RunResponse{
    uint16_t rc_ok;
    uint16_t seq;
  };

  struct APLXCommand{
    uint16_t cmd_run = SCP_CMD_APLX;
    uint16_t seq = 0;
    uint32_t address; // start address
  };
  struct APLXResponse{
    uint16_t rc_ok;
    uint16_t seq;
  };

#endif

  struct ReadCommand{
    uint16_t cmd_read = SCP_CMD_READ;
    uint16_t seq = 0;
    uint32_t address; // start address
    uint32_t length;  // length in bytes
    uint32_t type;    // 0 = Byte; 1 = Short; 2 = Word;
  };
  struct ReadResponse{
    uint16_t rc_ok;
    uint16_t seq;
    uint8_t data[256];
    uint32_t length;
  };

  struct WriteCommand{
    uint16_t cmd_write = SCP_CMD_Write;
    uint16_t seq = 0;
    uint32_t address; // start address
    uint32_t length;  // length in bytes
    uint32_t type;    // 0 = Byte; 1 = Short; 2 = Word;
    uint8_t data[256];
  };
  struct WriteResponse{
    uint16_t rc_ok;
    uint16_t seq;
  };
}

class SDP{

public:

  struct Address{
    uint8_t x;
    uint8_t y;
    uint8_t cpu;
    uint8_t port;
  };

  class Packet{

  public:

    Packet();
    Packet(SDP& sdp);

    // By setting flags to 0x87 the receiver will send back the exact same packet
    // otherwise the flag shout be set to 0x07
    // The ip-tag is used to map between a 8-bit address to an IPv4-address using a lookup table on the root chip
    void setFlagsTag(uint8_t flags, uint8_t tag);
    void setDest(uint8_t destX, uint8_t destY, uint8_t destCPU, uint8_t destPort);
    void setDest(Address& dest);
    void setSource(uint8_t sourceX, uint8_t sourceY, uint8_t sourceCPU, uint8_t sourcePort);
    void setSource(Address& source);
    void setCmd(uint16_t rc, uint16_t seq, uint32_t arg1, uint32_t arg2, uint32_t arg3);
    void setCmd(void* ptr, uint16_t size = 16);
    void setData(void* ptr, uint16_t size);

    uint8_t& flags();
    uint8_t& tag();
    uint8_t& dest_port_cpu();
    uint8_t& srce_port_cpu();
    uint16_t& dest_addr();
    uint16_t& srce_addr();

    uint16_t& cmd_rc();
    uint16_t& seq();
    uint32_t& arg1();
    uint32_t& arg2();
    uint32_t& arg3();

    uint8_t* dataPtr();

    uint16_t& size();

    void print(std::ostream& os);
    void printRaw();

  private:

    uint8_t data[SDP_HEADER_SIZE + SCP_HEADER_SIZE + SDP_DATA_SIZE + SDP_END_HEADER];

  private:

  };

  SDP(uint16_t localPort, uint16_t externalPort, char* externalIp);
  Address& sender();
  Address& receiver();
  void send(SDP::Packet* packet);
  void sendCommand(SDP::Packet* cmd, SDP::Packet* res);

  SCP::VersionResponse sendVersionCommand();
  SCP::ReadResponse sendReadCommand(uint32_t address, uint32_t length, uint32_t type);
  SCP::WriteResponse sendWriteCommand(uint32_t address, uint32_t length, uint32_t type, void* data);
#if SARK_VERSION >= 200
  SCP::RunResponse sendRunCommand(uint32_t address);
  SCP::APLXResponse sendAPLXCommand(uint32_t address);
#endif

private:

  struct sockaddr_in localAddr;
  struct sockaddr_in externalAddr;

  int sock;
  int slen;

  Packet commandPacket;
  Packet responsePacket;

  Address tx;
  Address rx;

};

std::ostream& operator<<(std::ostream& os, SDP::Packet& packet);

std::ostream& operator<<(std::ostream& os, SCP::VersionResponse& response);
std::ostream& operator<<(std::ostream& os, SCP::ReadResponse& response);
std::ostream& operator<<(std::ostream& os, SCP::WriteResponse& response);
#if SARK_VERSION >= 200
std::ostream& operator<<(std::ostream& os, SCP::RunResponse& response);
std::ostream& operator<<(std::ostream& os, SCP::APLXResponse& response);
#endif

#endif
