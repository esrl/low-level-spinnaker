#include<string.h>
#include<arpa/inet.h>
#include<sys/socket.h>

#include <iostream>

#include "../headers/UDPReceiver.hpp"
#include "../headers/SDPPacket.hpp"

int main(int argc, char** argv){

	if (argc != 2){
		std::cout << "Usage: receiver <udp port>" << std::endl;
		std::cout << "Try: receiver 17893" << std::endl;
		exit(1);
	}

	std::cout << "Starting sdp receiver on port: " << atoi(argv[1]) << std::endl;

	UDPReceiver receiver(atoi(argv[1]));

	SDPPacket packet;

	while (true){
		receiver.receive((char*)&packet, sizeof(SDPPacket));
		std::cout << packet << std::endl;
	}

	return 0;
}
