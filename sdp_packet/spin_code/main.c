#include <spin1_api.h>

void swap_sdp_hdr (sdp_msg_t *msg){
  uint dest_port = msg->dest_port;
  uint dest_addr = msg->dest_addr;

  msg->dest_port = msg->srce_port;
  msg->srce_port = dest_port;

  msg->dest_addr = msg->srce_addr;
  msg->srce_addr = dest_addr;
}

void sdp_send(){

}

void process_sdp (uint m, uint port){
  sdp_msg_t *msg = (sdp_msg_t *) m;

  io_printf(IO_STD, "SDP len %d, port %d - %s\n", msg->length, port, msg->data);
  io_printf(IO_BUF, "SDP");

  if (port == 1){
    uint key = 0xaaaa;
    uint data = 0x5555;
    io_printf(IO_STD, "Sending package: Key = %u, Data = %u\n", key, data);

    // Sending package
    pkt_tx_kd(key, data);

    swap_sdp_hdr (msg);
    (void) spin1_send_sdp_msg (msg, 10);
  }
  else if (port == 2){
    msg->tag = 1;				// IPTag 1
    msg->dest_port = PORT_ETH;		// Ethernet
    msg->dest_addr = sv->dbg_addr; 		// Root chip
    msg->flags = 0x07;			// Flags = 7
    msg->srce_port = spin1_get_core_id ();	// Source port
    msg->srce_addr = spin1_get_chip_id ();	// Source addr

    (void) spin1_send_sdp_msg (msg, 10);
  }

  spin1_msg_free (msg);

  if (port == 7){
	io_printf(IO_STD, "Exitting app!");
	io_printf(IO_BUF, "Exitting app!");
      spin1_exit (0);
  }
}

void flip_led (uint ticks, uint null){
  spin1_led_control (LED_INV (1));
}

void c_main (){
  io_printf (IO_STD, ">> sdping\n");

  spin1_set_timer_tick (1000000);

  //spin1_callback_on (TIMER_TICK, flip_led, 3);

  spin1_callback_on (SDP_PACKET_RX, process_sdp, 2);

  spin1_start (SYNC_NOWAIT);

  io_printf (IO_STD, ">> sdping done\n");
}
