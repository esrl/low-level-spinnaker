#include <UDPReceiver.hpp>

#include <string.h>

#include <iostream>

UDPReceiver::UDPReceiver(uint16_t port){
  //create a UDP socket
  if ((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) throw 1;

  // zero out the structure
  memset((char*) &socket_addr, 0, sizeof(socket_addr));

  socket_addr.sin_family = AF_INET;
  socket_addr.sin_port = htons(port);
  socket_addr.sin_addr.s_addr = htonl(INADDR_ANY);

  //bind socket to port
  if (bind(sock , (struct sockaddr*)&socket_addr, sizeof(socket_addr) ) == -1) throw 1;
}

uint16_t UDPReceiver::receive(char* data, uint16_t length){
  uint16_t size = 0;
  if ((size = recvfrom(sock, data, length, 0, (struct sockaddr *) &other, (socklen_t*)&slen)) == -1) return 0;
  return size;
}
