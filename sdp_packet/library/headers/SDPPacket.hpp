#ifndef SDP_PACKET_HPP
#define SDP_PACKET_HPP

#include <stdint.h>
#include <iostream>

#define SDP_HEADER_SIZE 10
#define SCP_HEADER_SIZE 16
#define DATA_SIZE 256

class SDPPacket{

public:

  SDPPacket();

  // By setting flags to 0x87 the receiver will send back the exact same packet
  // otherwise the flag shout be set to 0x07
  // The ip-tag is used to map between a 8-bit address to an IPv4-address using a lookup table on the root chip
  void setFlagsTag(uint8_t flags, uint8_t tag);
  void setDest(uint8_t destX, uint8_t destY, uint8_t destCPU, uint8_t destPort);
  void setSource(uint8_t sourceX, uint8_t sourceY, uint8_t sourceCPU, uint8_t sourcePort);

  void setCmd(uint16_t rc, uint16_t seq, uint32_t arg1, uint32_t arg2, uint32_t arg3);

  void setData(void* ptr, uint8_t size);

  void print(std::ostream& os);
  void printRaw();

private:

  uint8_t data[SDP_HEADER_SIZE + SCP_HEADER_SIZE + DATA_SIZE];

private:

};

std::ostream& operator<<(std::ostream& os, SDPPacket& packet);

#endif
