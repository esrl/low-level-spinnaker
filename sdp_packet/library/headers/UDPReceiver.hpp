#ifndef UDP_Receiver_HPP
#define UDP_Receiver_HPP

#include <stdint.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <string>

class UDPReceiver{

public:

  UDPReceiver(uint16_t port);
  uint16_t receive(char* data, uint16_t length);

private:

  struct sockaddr_in socket_addr, other;
  int sock;
  int i;
  int slen;
};

#endif
